defmodule Qazaq do
  @moduledoc """
    Qazaq transliteration

    Транслитерация для букв русского алфавита

    iex> Qazaq.transliterate("Строка на русском языке")
    "Stroka na russkom yazyke"

    Transliteration heavily based on rutils gem by Julian "julik" Tarkhanov and Co.
    <http://rutils.rubyforge.org/>
    Cleaned up and optimized.
  """

  defmacro __using__(_opts) do
    quote do
      import Qazaq
    end
  end

  @doc """
    Transliterate a string with russian characters
    Возвращает строку, в которой все буквы русского алфавита заменены на похожую по звучанию латиницу

    iex> Qazaq.transliterate "Строка на русском языке"
    "Stroka na russkom yazyke"
  """
  @spec transliterate(String.t) :: String.t
  def transliterate(text) do
    Qazaq.Transliteration.transliterate(text)
  end

  defmodule Transliteration do
    @moduledoc ~S"""
      Qazaq transliteration

      Транслитерация для букв казахского алфавита

      Transliteration heavily based on rutils gem by Julian "julik" Tarkhanov and Co.
      <http://rutils.rubyforge.org/>
      Cleaned up and optimized.
    """

    @lower_single %{
      "і" => "i", "ґ" => "g", "ё" => "yo", "№" => "#", "є" => "e",
      "ї" => "yi", "а" => "a", "ә" => "á", "б" => "b",
      "в" => "v", "г" => "g", "ғ" => "ǵ", "д" => "d", "е" => "e", "ж" => "zh",
      "з" => "z", "и" => "i", "й" => "y", "к" => "k", "қ" => "q", "л" => "l",
      "м" => "m", "н" => "n", "ң" => "ń", "о" => "o", "ө" => "ó", "п" => "p", "р" => "r",
      "с" => "s", "т" => "t", "у" => "u", "ұ" => "u", "ү" => "ú","ф" => "f", "х" => "h","һ" => "h",
      "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "'",
      "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya"
    }

    @lower_multi %{
      "ье" => "ie",
      "ьё" => "ie"
    }

    @uppper_single  %{
      "Ґ" => "G","Ё" => "YO","Є" => "E","Ї" => "YI","І" => "I",
      "А" => "A","Ә" => "Á","Б" => "B","В" => "V","Г" => "G","Ғ" => "Ǵ",
      "Д" => "D","Е" => "E","Ж" => "ZH","З" => "Z","И" => "I",
      "Й" => "Y","К" => "K","Қ" => "Q","Л" => "L","М" => "M","Н" => "N","Ң" => "Ń",
      "О" => "O","Ө" => "Ó","П" => "P","Р" => "R","С" => "S","Т" => "T",
      "У" => "U","Ұ" => "U", "Ү" => "Ú","Ф" => "F","Х" => "H","Һ" => "H","Ц" => "TS","Ч" => "CH",
      "Ш" => "SH","Щ" => "SCH","Ъ" => "'","Ы" => "Y","Ь" => "",
      "Э" => "E","Ю" => "YU","Я" => "YA"
    }

    @upper_multi %{
      "ЬЕ" => "IE",
      "ЬЁ" => "IE"
    }

    @lower Map.merge(@lower_single, @lower_multi)
    @upper Map.merge(@uppper_single, @upper_multi)

    multi_keys_reg = Map.merge(@lower_multi, @upper_multi) |> Map.keys |> Enum.join("|")
    @chars_regex ~r/(#{multi_keys_reg}|\w|.)/um

    @doc """
    Transliterate a string with qazaq characters

    Возвращает строку, в которой все буквы казахского алфавита заменены на похожую по звучанию латиницу
    """
    @spec transliterate(String.t) :: String.t
    def transliterate(str) do
      g = str |> String.graphemes
      Enum.map(g, fn x -> find(x) end)
    end

    defp find(char) do
      cond do
          Map.has_key?(@upper, char) ->
            @upper[char]
          Map.has_key?(@lower, char) ->
            @lower[char]
          true ->
            char
        end
    end

    defp trans_chars([]) do
      ""
    end

    defp trans_chars([[nil]]) do
      ""
    end

    defp trans_chars([[char]]) do
      trans_chars [[char], [nil]]
    end

    defp trans_chars([[char], [nchar] | tail]) do
      result =
        cond do
          Map.has_key?(@upper, char) && Map.has_key?(@lower, nchar) ->
            @upper[char] |> String.downcase |> String.capitalize
          Map.has_key?(@upper, char) ->
            @upper[char]
          Map.has_key?(@lower, char) ->
            @lower[char]
          true ->
            char
        end
      result <> trans_chars([ [nchar] | tail])
    end
  end
end
