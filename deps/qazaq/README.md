# Qazaq Elixir
Qazaq language transliteration support for Elixir.


## Installation
Add the latest stable release to your `mix.exs` file:

```elixir
defp deps do
  [
    {:qazaq, "~> 0.0.2"}
  ]
end
```

## Usage
```elixir
	Qazaq.transliterate "Қазақ тіліндегі жол"
	# => "Qazaq tilindegi jol"

```
