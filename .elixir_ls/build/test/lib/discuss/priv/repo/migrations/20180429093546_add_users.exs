defmodule Discuss.Repo.Migrations.AddUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :account, :string
      add :email, :string
      add :provider, :string
      add :token, :string
      add :object, :text
      timestamps()
    end
  end
end
