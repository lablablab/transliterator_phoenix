{application,ueberauth_google,
             [{description,"An Uberauth strategy for Google authentication."},
              {modules,['Elixir.Ueberauth.Strategy.Google',
                        'Elixir.Ueberauth.Strategy.Google.OAuth',
                        'Elixir.UeberauthGoogle']},
              {registered,[]},
              {vsn,"0.7.0"},
              {applications,[kernel,stdlib,elixir,logger,oauth2,ueberauth]}]}.
