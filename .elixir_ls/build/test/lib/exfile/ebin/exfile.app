{application,exfile,
             [{description,"File upload persistence and processing for Phoenix / Plug.\n"},
              {modules,['Elixir.Exfile','Elixir.Exfile.Backend',
                        'Elixir.Exfile.Backend.FileSystem',
                        'Elixir.Exfile.BackendTest','Elixir.Exfile.Config',
                        'Elixir.Exfile.Ecto',
                        'Elixir.Exfile.Ecto.CastContentType',
                        'Elixir.Exfile.Ecto.CastFilename',
                        'Elixir.Exfile.Ecto.File',
                        'Elixir.Exfile.Ecto.FileTemplate',
                        'Elixir.Exfile.Ecto.ValidateContentType',
                        'Elixir.Exfile.Ecto.ValidateFileSize',
                        'Elixir.Exfile.File','Elixir.Exfile.Hasher',
                        'Elixir.Exfile.Hasher.Random',
                        'Elixir.Exfile.Hasher.SHA256',
                        'Elixir.Exfile.Identify','Elixir.Exfile.LocalFile',
                        'Elixir.Exfile.Phoenix.Helpers',
                        'Elixir.Exfile.Processor',
                        'Elixir.Exfile.Processor.ContentType',
                        'Elixir.Exfile.Processor.FileSize',
                        'Elixir.Exfile.ProcessorChain',
                        'Elixir.Exfile.ProcessorRegistry',
                        'Elixir.Exfile.Router','Elixir.Exfile.Supervisor',
                        'Elixir.Exfile.Tempfile','Elixir.Exfile.Token',
                        'Elixir.Phoenix.HTML.Safe.Exfile.File']},
              {registered,[]},
              {vsn,"0.3.6"},
              {mod,{'Elixir.Exfile',[]}},
              {applications,[kernel,stdlib,elixir,logger,plug,crypto,inets]}]}.
