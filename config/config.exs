# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :discuss,
  ecto_repos: [Discuss.Repo]

# Configures the endpoint
config :discuss, DiscussWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pqVTWVlr7pgyEWv1hQ7aF159uUVjD0ZAtgXcnF1+QDwwgzX3Ix7kb64U2Bd4wyKh",
  render_errors: [view: DiscussWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Discuss.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :ueberauth, Ueberauth,
  providers: [
    github: { Ueberauth.Strategy.Github, [] }
  ]

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: "d8d9749117d1d37882d2",
  client_secret: "458bb0080f48c4d2f26a75441e136f28e64e56d0"

config :exfile, Exfile,
  secret: "158bb0080f48c4d2f26a55441e136f28e64e56d0",
  backends: %{
    "store" => {Exfile.Backend.FileSystem,
      directory: Path.expand("\\tmp\\store")
    },
    "cache" => {Exfile.Backend.FileSystem,
      directory: Path.expand("\\tmp\\cache")
    }
  }
