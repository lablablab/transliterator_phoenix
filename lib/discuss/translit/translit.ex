defmodule Discuss.Translit do
  def translit_file_en(fname) do
    if File.exists?(fname) do
      IO.puts("file exist: #{fname}. reading")
      case File.read(fname) do
        {:ok, body}      -> write("new.txt", body)
        {:error, reason} -> IO.puts("There was an error: #{reason}")
      end
    end
  end

  def translit_docx_en(file_name) do
    Enum.map(Docxelixir.read_paragraphs(file_name), fn x->Discuss.Translit.translit_en(x) end )
  end

  def translit_en(str) do
    x = String.graphemes(str)
    #    IO.inspect x
    String.graphemes(str)
    |> Enum.map(fn x  ->
      if Map.has_key?(cyr2lat, x) do
        Map.get(cyr2lat, x)
      else
        x
      end
    end)
    |> Enum.join("")
  end

  defp write(file, content) do
    translit = translit_en(content)
    case File.open(file, [:write]) do
      {:ok, nf}        ->
        IO.write(nf, translit)
        #IO.binwrite(nf, translit)
      {:error, reason} -> IO.puts("There was an error: #{reason}")
    end
  end

  defp cyr2lat do
    %{"," => ",",
      "." => ".",
      "!" => "!",
      "?" => "?",
      "@" => "@",
      "#" => "#",
      "$" => "$",
      "%" => "%",
      "^" => "^",
      "&" => "&",
      "*" => "*",
      "(" => "(",
      ")" => ")",
      "-" => "-",
      "_" => "_",
      "+" => "+",
      "=" => "=",
      "а" => "a",
      "ә" => "á",
      "б" => "b",
      "в" => "v",
      "г" => "g",
      "ғ" => "ǵ",
      "д" => "d",
      "е" => "e",
      "ё" => "ıo",
      "ж" => "j",
      "з" => "z",
      "и" => "ı",
      "й" => "ı",
      "к" => "k",
      "қ" => "q",
      "л" => "l",
      "м" => "m",
      "н" => "n",
      "ң" => "ń",
      "о" => "o",
      "ө" => "ó",
      "п" => "p",
      "р" => "r",
      "с" => "s",
      "т" => "t",
      "у" => "ý",
      "ұ" => "u",
      "ү" => "ú",
      "ф" => "f",
      "х" => "h",
      "һ" => "h",
      "ц" => "ts",
      "ч" => "ch",
      "ш" => "sh",
      "щ" => "sch",
      "ы" => "y",
      "і" => "i",
      "э" => "e",
      "ю" => "ıý",
      "я" => "ıa",
      "А" => "A",
      "Ә" => "Á",
      "Б" => "B",
      "В" => "V",
      "Г" => "G",
      "Ғ" => "Ǵ",
      "Д" => "D",
      "Е" => "E",
      "Ё" => "Io",
      "Ж" => "J",
      "З" => "Z",
      "И" => "I",
      "Й" => "I",
      "К" => "K",
      "Қ" => "Q",
      "Л" => "L",
      "М" => "M",
      "Н" => "N",
      "Ң" => "Ń",
      "О" => "O",
      "Ө" => "Ó",
      "П" => "P",
      "Р" => "R",
      "С" => "S",
      "Т" => "T",
      "У" => "Ý",
      "Ұ" => "U",
      "Ү" => "Ú",
      "Ф" => "F",
      "Х" => "H",
      "Һ" => "H",
      "Ц" => "Ts",
      "Ч" => "Ch",
      "Ш" => "Sch",
      "Ы" => "Y",
      "І" => "I",
      "Э" => "E",
      "Ю" => "Iý",
      "Я" => "Ia"
    }
  end
end
