defmodule DiscussWeb.PageController do
  use DiscussWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def upload(conn, params) do
    encoded = "ENCODED!"
    IO.inspect params
    render conn, "index.html", encoded: encoded
  end
end
