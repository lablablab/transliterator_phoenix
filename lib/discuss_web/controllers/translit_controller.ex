defmodule DiscussWeb.TranslitController do
  use DiscussWeb, :controller

  def index(conn, _) do
    render(conn, "index.html")
  end

  def new(conn, _) do
    #changeset = Image.changeset(%Image{})
    render(conn, "new.html")
  end

  def create(conn, params) do
    file_name = params["file"].filename
    ext = Path.extname(file_name)
    IO.puts ext
    source_file = params["file"].path
    #  Integer.to_string(conn.assigns.user.id) 
    prepare_dir! "files"
    dest_file = "files/" <> file_name
    source = File.read!(source_file)
    dest = File.stream!(dest_file)
    case ext do
      ".docx" ->
        source |> Discuss.Translit.translit_docx_en |> Stream.into(dest) |> Stream.run
      _ ->
        source |> Qazaq.transliterate |> Stream.into(dest) |> Stream.run
    end
    #render conn, "complete.html"
    #conn = put_resp_content_type(conn, "application/octet-stream", "utf-8")
    #Plug.Conn.send_file(conn, 200, dest_file)
    conn
      |> put_resp_content_type("application/octet-stream", "utf-8")
      |> put_resp_header("content-disposition", ~s[attachment; filename="#{file_name}"])
      |> send_file(200, dest_file)
  end

  defp prepare_dir!(dir) do
    unless File.exists?(dir) do
      File.mkdir!(dir)
    end
  end

end
