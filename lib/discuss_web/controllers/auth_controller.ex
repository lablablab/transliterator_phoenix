defmodule DiscussWeb.AuthController do
  use DiscussWeb, :controller
  plug Ueberauth

  alias DiscussWeb.User

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    object = Poison.encode!(auth)
    case auth.info.email do
      nil ->
        account = %{token: auth.credentials.token, email: "empty", account: auth.info.nickname, provider: "github", object: object}
        changeset1 = User.changeset(%User{}, account)
        signin(conn, changeset1)
      _ ->
        email = %{token: auth.credentials.token, email: auth.info.email, account: "empty", provider: "github", object: object}
        changeset2 = User.changeset(%User{}, email)
        signin(conn, changeset2)
      end
  end

  def signout(conn, _params) do
    conn
    |> configure_session(drop: true)
    |> redirect(to: topic_path(conn, :index))
  end

  defp signin(conn, changeset) do
    case insert_or_update_user(changeset) do
      {:ok, user} ->
        conn
        |> put_flash(:error, "Welcome back!")
        |> put_session(:user_id, user.id)
        |> redirect(to: topic_path(conn, :index))
      {:error, _reason} ->
        conn
        |> put_flash(:error, "Error sign in")
        |> redirect(to: topic_path(conn, :index))
    end
  end

  defp insert_or_update_user(changeset) do
    case changeset.changes.email do
      "empty" ->
        case Discuss.Repo.get_by(User, account: changeset.changes.account) do
          nil ->
            Discuss.Repo.insert(changeset)
          user ->
            {:ok, user}
        end
      _->
        case Discuss.Repo.get_by(Discuss.User, email: changeset.changes.email) do
          nil ->
            Discuss.Repo.insert(changeset)
          user ->
            {:ok, user}
        end
      end
  end
end
