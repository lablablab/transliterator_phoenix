defmodule DiscussWeb.CommentsChannel do
  use Phoenix.Channel
  alias DiscussWeb.Topic
  alias DiscussWeb.Comment

  def join("comments:" <> topic_id, _params, socket) do
    t_id = String.to_integer(topic_id)
    topic = Discuss.Repo.get(Topic, t_id) |> Discuss.Repo.preload(:comments)
    {:ok, %{comments: topic.comments}, assign(socket, :topic, topic)}
  end

  def handle_in(name, %{"content" => content}, socket) do
    IO.puts "--socket assigns-------------"
    IO.inspect socket.assigns
    IO.inspect socket.assigns.topic
    IO.inspect socket.assigns.user_id
    IO.puts "-----------------------------"
    topic = socket.assigns.topic
    user_id = socket.assigns.user_id

    changeset = topic
      |> Ecto.build_assoc(:comments, user_id: user_id)
      |> Comment.changeset(%{content: content})
    case Discuss.Repo.insert(changeset) do
      {:ok, comment} ->
        broadcast!(socket, "comments:#{socket.assigns.topic.id}:new", %{comment: comment})
        {:reply, :ok, socket}
      {:error, _reason} ->
        {:reply, {:error, %{errors: changeset}}, socket}
    end
  end
end
