defmodule DiscussWeb.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :account, :string
    field :email, :string
    field :provider, :string
    field :token, :string
    field :object, :string
    has_many :topics, DiscussWeb.Topic
    has_many :comments, DiscussWeb.Comment
    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:account, :email, :provider, :token, :object])
    |> validate_required([:object])
  end
end
